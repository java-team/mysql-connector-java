#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
VERSION=$2
DIR=mysql-connector-java-$VERSION.orig

# clean up the upstream tarball
tar zxf $3
rm $3
mv mysql-connector-java-$VERSION $DIR
XZ_OPT=--best tar cJf ../mysql-connector-java_$VERSION.orig.tar.xz -X debian/orig-tar.exclude $DIR
rm -rf $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
	. .svn/deb-layout
	mv $3 $origDir
	echo "moved $3 to $origDir"
fi

